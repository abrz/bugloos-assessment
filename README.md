# Bugloos Assessment



## INFO

This project has been implemented only for the assessment purpose with the main domain of API data mapping functionality and minimum of considerations

## Dependencies 

- php 7.4 and above
- docker 

## Get Started

- Give the execution permission to the start_db.sh file ( <code>sudo chmod +X start_db.sh</code> )
- Run the start_db.sh file to start mongodb ( <code>./start_db.sh</code> )
- Run composer install command ( <code>composer install</code> )
- Set the .env file values
- Run the index file in the web directory ( <code>php ./web/index.php</code> )
- if you want to see the data, Run the show file in the web directory ( <code>php -S localhost:2000 ./web/show.php</code> )

<span style="color:crimson">The file (.env) was added to the repo (wasn't a big deal)</span>

## API Sources
- jsonplaceholder.com
- reqres.in
- javatpoint.com