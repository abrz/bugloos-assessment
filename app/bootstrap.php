<?php

use App\Config;
use App\Mapper\DataMapper;
use App\Mapper\Model;
use DI\ContainerBuilder;
use Dotenv\Dotenv;

require __DIR__ . '/../vendor/autoload.php';

// Building Container with DI Container Builder
$containerBuilder = new ContainerBuilder;
$containerBuilder->addDefinitions([
    'env' => function() {
        $dotenv = Dotenv::createImmutable(dirname(__DIR__, 1));
        return $dotenv->load();
    },
    'config'  => DI\factory(function ($env) {
        $config = Config::getInstance();
        $config::setConfig($env['API_SOURCE']);
        return $config::getConfig();

    })->parameter('env', DI\get('env')),
    'db'  => DI\factory(function ($env){
        return new MongoDB\Client($env['DB_URL'].':'.$env['DB_PORT'], array("username" => $env['DB_USERNAME'], "password" => $env['DB_PASSWORD']));
    })->parameter('env', DI\get('env')),

    'mapper'  => DI\factory(function ($config) {
        return new DataMapper(new Model(), $config->get('mapping'));
    })->parameter('config', DI\get('config')),
]);
$container = $containerBuilder->build();
return $container;