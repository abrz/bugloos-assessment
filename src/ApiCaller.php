<?php 
namespace App;

use GuzzleHttp\Client;

class ApiCaller
{

    /**
     * Guzzle Http Client object
     *
     * @var [type]
     */
    private $http;

    /**
     * Where to get the API data
     *
     * @var [type]
     */
    private $endpoint;

    /**
     * Which method the API endpoint is accepting
     *
     * @var string
     */
    private $method = "GET";
    
    public function __construct(Client $http) {
        $this->http = $http;
    }

    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    public function getEndpoint()
    {
        return $this->endpoint;
    }

    public function setMethod($method)
    {
        if(!in_array( strtolower($method), ['get', 'post', 'put', 'patch', 'delete'])) {
            throw new \Exception("Given method is unknown", 1);
        } 
        $this->method = $method;
    }

    public function getMethod()
    {
        return $this->method;
    }

    
    public function call()
    {

        $response = $this->http->request($this->getMethod(), $this->getEndpoint() );
        return $response->getBody()->getContents();
    }


}