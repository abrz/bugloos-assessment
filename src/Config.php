<?php 
namespace App;

use Illuminate\Support\Collection;

class Config
{

    /**
     * instance of the self class
     *
     * @var [type]
     */
    private static $instance;
    
    /**
     * mapping config from the YAML file
     *
     * @var [type]
     */
    private static $config;

    /**
     * Singletone neccessary method
     *
     * @return self
     */
    public static function getInstance()
    {
        if(self::$instance) return self::$instance;
        return new self;
    }

    /**
     * Set the config from
     *
     * @param [type] $source
     * @return void
     */
    public static function setConfig($source)
    {   
        
        // Get the mapping.yaml file data 
        // this line should've been written dynamically based on the .env file (dynamic.yaml)
        $yml = yaml_parse_file( __DIR__ . '/../mapping.yaml');
        if(!isset($yml['api'][$source])) throw new \Exception("Given Source $source doesn't exist, please check the mapping.yaml file in the root file.", 1);
        // nested array to collection
        self::$config = Collection::make($yml['api'][$source])->map(function($item, $key) {
            if(is_array($item)) return Collection::make($item);
            return $item;
        });
    }

    /**
     * Get the config data
     *
     * @return Collection
     */
    public static function getConfig()
    {
        return self::$config;       
    }
    
}