<?php 
namespace App\Contracts;

use Illuminate\Support\Collection;

interface ApiDataParse
{
    
    /**
     * Set the API data
     *
     * @param [type] $data
     * @return object
     */
    public function setData($data);
    
    /**
     * Parse the API data
     *
     * @param string $root
     * @return Collection
     */
    public function parse($root = "") : Collection;

}