<?php 
namespace App\Mapper;

use Illuminate\Support\Collection;

class DataMapper
{
    
    /**
     * Base data that we want to map
     *
     * @var [type]
     */
    private $schema;

    /**
     * Base model
     *
     * @var [type]
     */
    private $model;

    /**
     * Instructions of mapping
     *
     * @var [type]
     */
    public $config;
    
    /**
     * Constructor of the class
     *
     * @param Model $model
     * @param Collection $config
     */
    public function __construct(Model $model, Collection $config) {
        $this->model = $model;
        $this->config = $config;
    }

    /**
     * Set schema of the source data
     *
     * @param object $schema
     * @return void
     */
    public function setSchema(object $schema)
    {
        $this->schema = $schema;
        return $this;
    }

    /**
     * Get the schema
     *
     * @return object
     */
    public function getSchema() : object
    {
        return $this->schema;
    }

    /**
     * Map the schema to the base mode based on the config
     *
     * @return Model
     */
    public function mapToModel()
    {
        $this->config->each(function($item, $key) {
            $this->model->setAttr(
                $key,
                $this->getSchema()->$item
            );
        });

        return $this->model;
    }   

}