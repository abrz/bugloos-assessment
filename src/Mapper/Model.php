<?php 
namespace App\Mapper;

use Illuminate\Support\Collection;

class Model
{
    /**
     * Attributes for this model
     *
     * @var array
     */
    private array $attributes;

    /**
     * Set new attribute of the model
     *
     * @param [type] $key
     * @param [type] $value
     * @return self
     */
    public function setAttr($key, $value)    
    {
        $this->attributes[$key] = $value;
        return $this;
    }

    /**
     * Get the attibutes of the moel
     *
     * @return Collection
     */
    public function getAttr() : Collection
    {
        return Collection::make($this->attributes);
    }

}