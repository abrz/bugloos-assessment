<?php 
namespace App\Parser;

use App\Contracts\ApiDataParse;

class DataParser
{

    /**
     * parse the data
     *
     * @param ApiDataParse $parser
     * @param string $root
     * @return void
     */
    public function parse(ApiDataParse $parser, $root = "")
    {
        return $parser->parse($root);
    }

}