<?php 
namespace App\Parser;

use App\Contracts\ApiDataParse;
use Exception;

class DataParserFactory
{

    /**
     * Build the data parser
     *
     * @param [type] $type
     * @return ApiDataParse
     */
    public static function build($type) : ApiDataParse
    {
        return match ($type) {
            'json'  => new JsonDataParser(),
            'xml'   => new XmlDataParser(),
            default => throw new Exception("Unknown Parser Type", 1)
        };
    }

}