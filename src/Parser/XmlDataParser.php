<?php 
namespace App\Parser;

use App\Contracts\ApiDataParse;
use Illuminate\Support\Collection;

class XmlDataParser implements ApiDataParse
{
    
    /**
     * what we want to parse
     *
     * @var [type]
     */
    private $data;

    /**
     * Set the data
     *
     * @param [type] $data
     * @return Self
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Concrete of the XML data parser
     *
     * @param string $root
     * @return Collection
     */
    public function parse($root = "") : Collection
    {
        $data = (array) simplexml_load_string($this->data);
        $data = $root ? $data[$root] : $data; 
        return Collection::make($data);
    }


}