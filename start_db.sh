#! /bin/bash

if [ ! "$(docker ps -a -q -f name=mongo-test)" ]; then 
    if ["$(docker ps -aq -f status=exited -f name=mongo-test )"]; then
        docker rm mongo-test
    fi
    
    docker run -d --name mongo-test -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=secret mongo

else
    docker start mongo-test
fi
