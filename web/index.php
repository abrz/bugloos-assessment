<?php
// Bootstrap the app

use App\ApiCaller;
use App\Parser\DataParser;
use App\Parser\DataParserFactory;

$container = require __DIR__ . '/../app/bootstrap.php';

// load .env file
$env = $container->get("env");
// get the config class and interact with mapping.yml file
$config = $container->get("config");

// I chose mongo for this app just for representing the idea of storing to db
// Our DB should be implemented driver base which is a little bit time-consuming and out of scope of this assesment
// For simplicity we use mongo 
// checkout the data.php in the current directory
$connection = $container->get('db');
$database = $connection->test;

// Call the API
$apiCaller = $container->get(ApiCaller::class);
$apiCaller->setEndpoint($config->get('endpoint'));
$apiCaller->setMethod($config->get('method'));
$apiData = $apiCaller->call();

// Build DataParser based on datatype config (JSON | XML)
$chooseParser = DataParserFactory::build($config->get('datatype'));
$chooseParser->setData($apiData);

// Parse the data
$parser = $container->get(DataParser::class);
$data = $parser->parse($chooseParser, $config->get('root'));


/**
 * Json data type test
 * we just mapping first item of the collection 
 * of course we can iterate ove the data 
 * (these code should be implemented in controller)
 */
$mapper = $container->get('mapper');
$mapper->setSchema($data->first());

//insert into db
$database->jsondata->insertOne(
    $mapper->mapToModel()->getAttr()->toArray()
);

/**
 * XML data type test 
 * only a simple payload
*/
// $database->xmldata->insertOne(
//     $mapper->mapToModel()->getAttr()->toArray()
// );

echo <<<EOL
    1. API Called
    2. Data parsed
    3. Data stored into the database (Mongo for simplicity)
EOL;