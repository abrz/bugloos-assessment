<?php
$container = require __DIR__ . '/../app/bootstrap.php';
$connection = $container->get('db');
$database = $connection->test;

$jsonData = $database->jsondata->findOne(
    [],
    [
        'sort' => ['pop' => -1],
    ]
);
$xmlData = $database->xmldata->findOne(
    [],
    [
        'sort' => ['pop' => -1],
    ]
);


if(isset($_GET['clean_db']) && $_GET['clean_db'] == true){
    $database->jsondata->drop();
    $database->xmldata->drop();
    header('Location: http://localhost:2000');
    exit;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Show inserted data</title>

    <style>

            td,  th {
            border: 1px solid #ddd;
            padding: 8px;
            }

             tr:nth-child(even){background-color: #f2f2f2;}

             tr:hover {background-color: #ddd;}

             th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #04AA6D;
            color: white;
            }
    </style>
</head>
<body>
    
    <table>
        <thead>
            Json data
        </thead>
        <?php
            if($jsonData){

                foreach ($jsonData as $key => $value) {
                    echo '<tr>
                        <th>'.$key.'</th>
                        <td>'.$value.'</td>
                    </tr>';
                }
            }else{
               echo "NO RECOURD FOUND";
            }
        ?>
    </table>
            <br>
    <table>
        <thead>xml data</thead>
        <?php
  
            if($xmlData){
                foreach ($xmlData as $key => $value) {
                   
                    echo '<tr>
                        <th>'.$key.'</th>
                        <td>'.json_encode($value).'</td>
                    </tr>';
                }
            }else{
                echo "NO RECOURD FOUND";
            }
        ?>
    </table>
    

    <form action="" method="get">
        <button name="clean_db" type="submit" value="true">Clean DB</button>
    </form>
</body>
</html>